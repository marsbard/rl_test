COUNTER_FILE=/tmp/runtail_counter.tmp
COUNTER_LOCK=/tmp/runtail_counter.lock

OUT_LOGFILE=/tmp/errchecker-log.txt
OUT_LOCK=/tmp/errchecker-log.lock

RUNTAIL_SEM=/tmp/runtail_semaphore.tmp

FLOCK_TIMEOUT=0.3



function enable_runtail {
  touch $RUNTAIL_SEM
}

function end_runtail {
  rm $RUNTAIL_SEM
}

function run_tail {
  errcho "run_tail: $1"
  local FILE_TO_TAIL=$1
  local LINE

  while read LINE
  #tail -F $FILE_TO_TAIL | while read LINE
  do

    if [ ! -f $RUNTAIL_SEM ]
    then
      break
    fi

    OUT=`checker $LINE`
    RES=$?
    if [ $RES = 1 ]; then incr_count; fi
    if [ $RES = 2 ]
    then
      COUNT=`get_count`
      OUT="$OUT - Number of exceptions: $COUNT"
    fi
    if [ $RES != 0 ]
    then
      write_log "$2 - $OUT" &
    fi

  done < <(tail -F $FILE_TO_TAIL)

}


function write_log {
  errcho "write_log: $*"
  (
    flock -w $FLOCK_TIMEOUT 98

    echo "[`date`][$$] $*" >> $OUT_LOGFILE

  ) 98>${OUT_LOCK}
}

function incr_count {
  errcho "incr_count:"
  (
    flock -w $FLOCK_TIMEOUT 99

    if [ ! -f ${COUNTER_FILE} ]
    then
      echo 0 > ${COUNTER_FILE}
      COUNT=0
    else
      COUNT=`cat ${COUNTER_FILE}`

      # if it is an empty string initialise it
      if [ -z $COUNT ]; then COUNT=0; fi
    fi
    # errcho "incr_count: COUNT='$COUNT'"

    # increment the count
    COUNT=$(( $COUNT + 1 ))
    errcho "incr_count: COUNT='$COUNT'"

    echo $COUNT > ${COUNTER_FILE}

  ) 99>${COUNTER_LOCK}

}

function set_count {
  errcho "set_count: $1"
  (
    flock -w $FLOCK_TIMEOUT 99

    echo $1 > ${COUNTER_FILE}

  ) 99>${COUNTER_LOCK}

}

function get_count {
  (
    flock -w $FLOCK_TIMEOUT 99
    if [ ! -f ${COUNTER_FILE} ]
    then
      echo 0 > ${COUNTER_FILE}
      COUNT=0
    else
      COUNT=`cat ${COUNTER_FILE}`

      # if it is an empty string initialise it
      if [ -z $COUNT ]; then COUNT=0; fi
    fi

    errcho "get_count: count is $COUNT"
    echo $COUNT

  ) 99>${COUNTER_LOCK}
}





# Usage: echo <possibly multline> | checker
# will only return lines with Exception: or ERROR
# and increment counter when found.
# If "INFO: Server startup in:" is found then
# counter is reset and outputs "TOMCAT IS STARTED"
#
# returns 0 if nothing interesting happened
# returns 1 if there is an exception
# returns 2 if we find TOMCAT IS STARTED
EXCEPTION_COUNT=0
function checker {
  input=$*
  echo $input | grep "Exception:" 2>&1 > /dev/null
  if [ $? = 0 ]
  then
    echo "$input"
    return 1
  fi
  echo $input | grep "ERROR" 2>&1 > /dev/null
  if [ $? = 0 ]
  then
    echo "$input"
    return 1
  fi
  echo $input | grep "INFO: Server startup in:" 2>&1 > /dev/null
  if [ $? = 0 ]
  then
    echo "TOMCAT IS STARTED"
    return 2
  fi
  return 0
}

# Usage: detect_log_files
# Expects $WATCH_LOG_DIR to be set as a global envar
# Sets global envars:
#   $LOGFILES - an array of all currently found log files
#   $NEW_FILES - an array of the new files found this run
#   $GONE_FILES - an array of files which have disappeared
function detect_log_files {

  OIFS=$IFS


  errcho detect_log_files: Starting detect_log_files
  errcho "detect_log_files: LOGFILES at start = ${LOGFILES[@]}"

  FILELIST="`find ${WATCH_LOG_DIR} ! -type d | sed "s;^${WATCH_LOG_DIR};;"`"

  errcho "detect_log_files: FILELIST='$FILELIST'"
  errcho "detect_log_files: LAST_FILELIST='$LAST_FILELIST'"
  DIFF=`diff <(echo "$FILELIST") <(echo "$LAST_FILELIST")`

  NEW_FILELIST=`printf "$DIFF" | grep "^<" | sed "s/^< //"`
  errcho "detect_log_files: NEW_FILELIST='${NEW_FILELIST}'"

  GONE_FILELIST=`printf "$DIFF" | grep "^>" | sed "s/^> //"`
  errcho "detect_log_files: GONE_FILELIST='${GONE_FILELIST}'"

  errcho "detect_log_files: make FILES array"
  make_array "$FILELIST"
  IFS=""
  FILES=(${_ARR[@]})
  IFS=$OIFS

  errcho "detect_log_files: make NEW_FILES array"
  make_array "$NEW_FILELIST"
  IFS=""
  NEW_FILES=(${_ARR[@]})
  IFS=$OIFS

  errcho "detect_log_files: make GONE_FILES array"
  make_array "$GONE_FILELIST"
  IFS=""
  GONE_FILES=(${_ARR[@]})
  IFS=$OIFS

  IFS=""
  LOGFILES=(${FILES[@]})
  IFS=$OIFS

  # save this for next time
  LAST_FILELIST=$FILELIST

}


# Usage: contains <arrname> <element>
# returns 0 if element contained in array, 1 otherwise
function contains {
  arrname=$1[@]
  shift
  element=$*
  _OIFS=$IFS
  IFS=""
  arr=(${!arrname})
  IFS=$_OIFS

  errcho "contains: arrname=$arrname arr=${arr[@]}"

  for i in `seq 0 $(( ${#arr[@]} - 1 ))`
  do
    errcho "contains: i=$i arr[i]='${arr[$i]}' element='$element'"
    if [ "${arr[$i]}" == "$element" ]
    then
      return 0
    fi
  done
  return 1
}



# Usage: make_array <multiline string>
# make an array from multiline string
# and set it as global _ARR
_ARR=()
function make_array {
  errcho "make_array: --- called with \$1=$1"

  local COUNT=0
  local LINE
  unset _ARR
  _ARR=()

  while read LINE
  do
    errcho "make_array: LINE=$LINE"
    # generally skip blank lines
    if [ ! -z "$LINE" ]
    then
      errcho "make_array: adding '$LINE' to array[$COUNT]"
      _ARR[$COUNT]="$LINE"
      errcho "make_array: _ARR[$COUNT]=${_ARR[COUNT]}"
      COUNT=$(( $COUNT + 1 ))
    fi

    errcho "make_array:_ARR='${_ARR[@]}'"
    errcho "make_array:_ARR count=${#_ARR[@]}"

    for i in `seq 0 $(( ${#_ARR[@]} - 1))`
    do
      errcho "make_array:_ARR[$i] = '${_ARR[$i]}'"
    done

  done < <(echo "${1}")
}
