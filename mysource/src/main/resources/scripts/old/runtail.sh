#!/bin/bash

# runtail.sh - to be run as a standalone shell script rather than as an
# include.
#
# The work loop will maintain a number of these jobs in the background

# resolve absolute path of argument
FILE_TO_TAIL=`readlink -e $1`

COUNTER_FILE=/tmp/runtail_counter.tmp
COUNTER_LOCK=/tmp/runtail_counter.lock

OUT_LOGFILE=/tmp/errchecker-log.txt
OUT_LOCK=/tmp/errchecker-log.lock

FLOCK_TIMEOUT=.3


trap 'exit 99' INT


cd "`dirname $0`"
source "funcs.sh"

# I would like to use errcho
source "tests/assert.sh"

function write_log {
  errcho "write_log: $*"
  (
    flock -w $FLOCK_TIMEOUT 98

    echo "[`date`][$$] $*" >> $OUT_LOGFILE

  ) 98>${OUT_LOCK}
}

function incr_count {
  errcho "incr_count:"
  (
    flock -w $FLOCK_TIMEOUT 99

    COUNT=`cat ${COUNTER_FILE}`
    # errcho "incr_count: COUNT='$COUNT'"

    # if it is an empty string initialise it
    if [ -z $COUNT ]
    then
      COUNT=0
    fi

    # errcho "incr_count: COUNT='$COUNT'"

    # increment the count
    COUNT=$(( $COUNT + 1 ))
    # errcho "incr_count: COUNT='$COUNT'"

    echo $COUNT > ${COUNTER_FILE}

  ) 99>${COUNTER_LOCK}

}

function set_count {
  errcho "set_count: $1"
  (
    flock -w $FLOCK_TIMEOUT 99

    echo $1 > ${COUNTER_FILE}

  ) 99>${COUNTER_LOCK}

}

function get_count {
  (
    flock -w $FLOCK_TIMEOUT 99
    if [ ! -f ${COUNTER_FILE} ]
    then
      echo 0 > ${COUNTER_FILE}
      COUNT=0
    else
      COUNT=`cat ${COUNTER_FILE}`

      # if it is an empty string initialise it
      if [ -z $COUNT ]; then COUNT=0; fi
    fi

    errcho "get_count: count is $COUNT"
    echo $COUNT

  ) 99>${COUNTER_LOCK}
}



while read LINE
do
  OUT=`checker $LINE`
  RES=$?
  if [ $RES = 1 ]; then incr_count; fi
  if [ $RES = 2 ]
  then
    COUNT=`get_count`
    OUT="$OUT - Number of exceptions: $COUNT"
  fi
  if [ $RES != 0 ]
  then
    write_log $OUT
  fi

done < <(tail -F $FILE_TO_TAIL &)
