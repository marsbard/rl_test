
cd "`dirname $0`"

source "assert.sh"

FLOCK_TIMEOUT=.3
OUT_LOCK=/tmp/errchecker-log.lock
WIN="INFO: Server startup in:"

errcho "write_loglines.sh: "
# get some lines without the winline in
FAKELOG=../../../../test/resource/tinylog.txt
FAKELOGLINES=""
SEP=""
while read LINE
do
  echo $LINE | grep "$WIN" 2>&1 > /dev/null
  if [ $? != 0 ]
  then
      FAKELOGLINES=`printf "${FAKELOGLINES}${SEP}${LINE}"`
      SEP="\n"
  fi
done < <(cat $FAKELOG)


errcho "write_loglines.sh: starting write"

FILE=$1
CONTINUE=1
#( flock -n -w $FLOCK_TIMEOUT 97
  while [ $CONTINUE = 1 ]
  do
    echo "$FAKELOGLINES" | while read LINE
    do
      echo "$2 - $LINE" >> $FILE
      # sleep for a random fraction of a second (up to 0.32767)
      sleep ".${RANDOM}"
    done
  done
#) 97>${OUT_LOCK}
