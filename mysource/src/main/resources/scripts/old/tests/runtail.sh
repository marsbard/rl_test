cd "`dirname $0`"
#
source "../funcs.sh"

source "assert.sh"


COUNTER_FILE=/tmp/runtail_counter.tmp
COUNTER_LOCK=/tmp/runtail_counter.lock

OUT_LOGFILE=/tmp/errchecker-log.txt
OUT_LOCK=/tmp/errchecker-log.lock

FLOCK_TIMEOUT=0.3

WIN="INFO: Server startup in:"

# get some lines without the winline in
FAKELOG=../../../../test/resource/tinylog.txt
FAKELOGLINES=""
SEP=""
while read LINE
do
  echo $LINE | grep "$WIN" 2>&1 > /dev/null
  if [ $? != 0 ]
  then
      FAKELOGLINES=`printf "${FAKELOGLINES}${SEP}${LINE}"`
      SEP="\n"
  fi
done < <(cat $FAKELOG)


# echo not a real test yet, wait a sec and then ctrl c
# echo
# echo then you can look at /tmp/errchecker-log.txt and
# echo /tmp/runtail_counter.tmp
#
# ../runtail.sh $FAKELOG

function write_loglines {
  FILE=$1
  CONTINUE=1
  ( flock -w $FLOCK_TIMEOUT 98
    while [ $CONTINUE = 1 ]
    do
      echo "$FAKELOGLINES" | while read LINE
      do
        echo "$2 - $LINE" >> $FILE
        # sleep for a random fraction of a second (up to 0.32767)
        sleep ".${RANDOM}"
      done
    done
  ) 98>${OUT_LOCK}
}

function reset_state {
  rm -f /tmp/errchecker-log.* \
    /tmp/runtail_counter.* \
    /tmp/foobar*.txt
}





reset_state
enable_runtail

write_loglines /tmp/foobar1.txt WLL1_PID &
#bash ./write_loglines.sh /tmp/foobar1.txt WLL1_PID &

WLL1_PID=$!
echo WLL1_PID=$WLL1_PID

write_loglines /tmp/foobar2.txt WLL2_PID &
#bash ./write_loglines.sh /tmp/foobar1.txt WLL2_PID &

WLL2_PID=$!
echo WLL2_PID=$WLL2_PID

run_tail /tmp/foobar1.txt RT1_PID &
#../runtail.sh /tmp/foobar1.txt &
RT1_PID=$!
echo RT1_PID=$RT1_PID

run_tail /tmp/foobar2.txt RT2_PID &
#../runtail.sh /tmp/foobar2.txt &
RT2_PID=$!
echo RT2_PID=$RT2_PID


sleep 15
echo Killing WLL1_PID:$WLL1_PID WLL2_PID:$WLL2_PID
kill $WLL1_PID $WLL2_PID

echo Killing RT1_PID:$RT1_PID RT2_PID:$RT2_PID
kill $RT1_PID $RT2_PID

# append the win line to one of the files and read it
# haven't been counting the logs we've written but anyway the exception count
# should be very non-zero and possibly (probably?) different every run
echo $WIN >> /tmp/foobar1.txt
run_tail /tmp/foobar1.txt RT_PID_post &
RT_PID=$!

sleep 1
kill $RT_PID

set -vx
procs="$(jobs -p)"
for p in $procs
do
  ps ax | grep $p
done
echo "Kill: $procs"
# Ignore process that are already dead
kill $procs #2> /dev/null

#tail /tmp/errchecker-log.txt

#echo $0 $*

#kill `ps ax | grep "$*" | grep -v grep | cut -c1-5`

end_runtail


#reset_state
