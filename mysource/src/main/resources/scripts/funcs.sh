
# temporary file where we store the exception count, and its lock
COUNTER_FILE=/tmp/runtail_counter.tmp
COUNTER_LOCK=/tmp/runtail_counter.lock

# lock for our output log (OUT_LOGFILE is defined in errchecker)
OUT_LOCK=/tmp/errchecker-log.lock

# Wait 1/3 of a second before retrying if we find we are locked
FLOCK_TIMEOUT=0.3

# Usage: taildiff <FILENAME> <PREVIOUS SIZE>
# Get the difference between a file's last size and its current size.
# Returns "%d:%s" where %d is an integer representing the new size
# and %s is the actual difference in content from previously.
#
# Not used in the system, only in the tests
function taildiff {

  FNAME=$1
  LAST_SZ=$2

  # calculate current size
  local -i SZ
  SZ=`wc -c $1 | cut -f1 -d\ `

  errcho "taildiff: new size $SZ"

  if [ $SZ -gt $LAST_SZ ]
  then
    local -i DIFF=$(( $SZ - $LAST_SZ ))
    errcho SZ2 is bigger, tailing ${DIFF} more bytes

    # "%d:%s" %d is size, %s is changed content
    echo -n "${SZ}:"
    tail --bytes=$DIFF $FNAME
  else
    echo "0:"
  fi
}

# Usage: end_tail $FILENAME
# Kill off a running runtail based on the filename that was used
# to start it. Searches process table for "tail" command matching
# known invoication
function end_tail {
  errcho "end_tail: \$1=$1"

  local PID=`ps ax | grep "${TAILCMD} $1 -F" | grep -v grep | cut -c1-5`
  errcho "end_tail: PID=$PID"
  if [ ! -z "$PID" ]
    then
    kill $PID 2>&1 > /dev/null
  fi
}

# Usage: run_tail $FILENAME
# Start a background tail process to monitor $FILENAME using
# checker() - when checker returns $INTERESTING then we send
# whatever it has output to the flock-protected output log file
function run_tail {
  errcho "run_tail: $1"
  local FILE_TO_TAIL=$1
  local LINE

  while read LINE
  do
    errcho "run_tail: LINE=$LINE"

    OUT=`checker $LINE`
    RES=$?
    errcho "run_tail: RES=$RES OUT=$OUT"
    if [ $RES = 1 ]; then incr_count; fi
    if [ $RES = 2 ]
    then
      COUNT=`get_count`
      OUT="$OUT - Number of exceptions: $COUNT"
      set_count 0
    fi
    if [ $RES != 0 ]
    then
      write_log "$2 - $OUT"
    fi

  done < <(${TAILCMD} "$FILE_TO_TAIL" -F)

}

# Usage: write_log: <some message> (<more text> <blah>...)
# Writes a log message to ${OUT_LOGFILE}. Protected by flock so
# multiple processes cannot interleave messages. Prepends current
# date and process id of process writing to logfile.
function write_log {
  errcho "write_log: $*"
  (
    flock -w $FLOCK_TIMEOUT 98

    echo "[`date`][$$] $*" >> $OUT_LOGFILE

  ) 98>${OUT_LOCK}
}

# Usage: incr_count
# Increments the exception counter. Retrieves current result and stores
# incremented result in temp file under flock
function incr_count {
  errcho "incr_count:"
  (
    flock -w $FLOCK_TIMEOUT 99

    if [ ! -f ${COUNTER_FILE} ]
    then
      echo 0 > ${COUNTER_FILE}
      COUNT=0
    else
      COUNT=`cat ${COUNTER_FILE}`

      # if it is an empty string initialise it
      if [ -z $COUNT ]; then COUNT=0; fi
    fi

    COUNT=$(( $COUNT + 1 ))
    errcho "incr_count: COUNT='$COUNT'"

    echo $COUNT > ${COUNTER_FILE}

  ) 99>${COUNTER_LOCK}

}

# Usage: set_count <int value>
# Sets the excception counter to a specific value and stores
# the result in a temp file protected by flock
function set_count {
  errcho "set_count: $1"
  (
    flock -w $FLOCK_TIMEOUT 99

    echo $1 > ${COUNTER_FILE}

  ) 99>${COUNTER_LOCK}

}

# Usage: get_count
# Get the current exception count from the temp file
function get_count {
  (
    flock -w $FLOCK_TIMEOUT 99
    if [ ! -f ${COUNTER_FILE} ]
    then
      echo 0 > ${COUNTER_FILE}
      COUNT=0
    else
      COUNT=`cat ${COUNTER_FILE}`

      # if it is an empty string initialise it
      if [ -z $COUNT ]; then COUNT=0; fi
    fi

    errcho "get_count: count is $COUNT"
    echo $COUNT

  ) 99>${COUNTER_LOCK}
}


# Usage: echo <line> | checker
# will only return lines with Exception: or ERROR
# and increment counter when found.
# If "INFO: Server startup in:" is found then
# counter is reset and outputs "TOMCAT IS STARTED"
#
# returns 0 if nothing interesting happened
# returns 1 if there is an exception
# returns 2 if we find "INFO: Server startup in:"
EXCEPTION_COUNT=0
function checker {
  input=$*
  echo $input | grep "Exception:" 2>&1 > /dev/null
  if [ $? = 0 ]
  then
    echo "$input"
    return 1
  fi
  echo $input | grep "ERROR" 2>&1 > /dev/null
  if [ $? = 0 ]
  then
    echo "$input"
    return 1
  fi
  echo $input | grep "INFO: Server startup in:" 2>&1 > /dev/null
  if [ $? = 0 ]
  then
    echo "TOMCAT IS STARTED"
    return 2
  fi
  return 0
}

# Usage: detect_log_files
# Expects $WATCH_LOG_DIR to be set as a global envar
# Sets global envars:
#   $LOGFILES - an array of all currently found log files
#   $NEW_FILES - an array of the new files found this run
#   $GONE_FILES - an array of files which have disappeared
function detect_log_files {

  OIFS=$IFS


  errcho detect_log_files: Starting detect_log_files
  errcho "detect_log_files: LOGFILES at start = ${LOGFILES[@]}"


  # funky hack... use diff to compare the output of find between
  # runs to produce NEW_FILELIST and GONE_FILEIST
  FILELIST="`find ${WATCH_LOG_DIR} ! -type d | sed "s;^${WATCH_LOG_DIR};;"`"
  DIFF=`diff <(echo "$FILELIST") <(echo "$LAST_FILELIST")`
  NEW_FILELIST=`printf "$DIFF" | grep "^<" | sed "s/^< //"`
  GONE_FILELIST=`printf "$DIFF" | grep "^>" | sed "s/^> //"`

  errcho "detect_log_files: FILELIST='$FILELIST'"
  errcho "detect_log_files: LAST_FILELIST='$LAST_FILELIST'"
  errcho "detect_log_files: NEW_FILELIST='${NEW_FILELIST}'"
  errcho "detect_log_files: GONE_FILELIST='${GONE_FILELIST}'"

  errcho "detect_log_files: make FILES array"
  make_array "$FILELIST"
  IFS=""
  FILES=(${_ARR[@]})
  IFS=$OIFS

  errcho "detect_log_files: make NEW_FILES array"
  make_array "$NEW_FILELIST"
  IFS=""
  NEW_FILES=(${_ARR[@]})
  IFS=$OIFS

  errcho "detect_log_files: make GONE_FILES array"
  make_array "$GONE_FILELIST"
  IFS=""
  GONE_FILES=(${_ARR[@]})
  IFS=$OIFS

  IFS=""
  LOGFILES=(${FILES[@]})
  IFS=$OIFS

  # save this for next time
  LAST_FILELIST=$FILELIST

}


# Usage: contains <arrname> <element>
# returns 0 if element contained in array, 1 otherwise
function contains {
  arrname=$1[@]
  shift
  element=$*
  _OIFS=$IFS
  IFS=""
  arr=(${!arrname})
  IFS=$_OIFS

  errcho "contains: arrname=$arrname arr=${arr[@]}"

  for i in `seq 0 $(( ${#arr[@]} - 1 ))`
  do
    errcho "contains: i=$i arr[i]='${arr[$i]}' element='$element'"
    if [ "${arr[$i]}" == "$element" ]
    then
      return 0
    fi
  done
  return 1
}



# Usage: make_array <multiline string>
# make an array from multiline string
# and set it as global _ARR
_ARR=()
function make_array {
  errcho "make_array: --- called with \$1=$1"

  local COUNT=0
  local LINE
  unset _ARR
  _ARR=()

  while read LINE
  do
    errcho "make_array: LINE=$LINE"
    # generally skip blank lines
    if [ ! -z "$LINE" ]
    then
      errcho "make_array: adding '$LINE' to array[$COUNT]"
      _ARR[$COUNT]="$LINE"
      errcho "make_array: _ARR[$COUNT]=${_ARR[COUNT]}"
      COUNT=$(( $COUNT + 1 ))
    fi

    errcho "make_array:_ARR='${_ARR[@]}'"
    errcho "make_array:_ARR count=${#_ARR[@]}"

    for i in `seq 0 $(( ${#_ARR[@]} - 1))`
    do
      errcho "make_array:_ARR[$i] = '${_ARR[$i]}'"
    done

  done < <(echo "${1}")
}
