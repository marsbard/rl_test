

cd "`dirname $0`"

source "../funcs.sh"
source "assert.sh"

# this var is expected to be set as a global before running detect_log_files
WATCH_LOG_DIR="../tmplog"

# when running this command we expect at the end of it a global array of
# paths to log files "$LOGFILES[]"

# let's start with no files in the log dir
rm -rf $WATCH_LOG_DIR
mkdir -p $WATCH_LOG_DIR

detect_log_files

# now we expect the logfile count to be zero

assertEquals "no files there to start" 0 "${#LOGFILES[@]}" $LINENO

# create a file, redetect and see that we get a count now

touch "${WATCH_LOG_DIR}/FILE1"
touch "${WATCH_LOG_DIR}/FILE1a"
detect_log_files
assertEquals "two files found" 2 "${#LOGFILES[@]}" $LINENO
#assertEquals "name is FILE1" "/FILE1" "${LOGFILES[0]}" $LINENO

contains LOGFILES "/FILE1"
RES=$?
assertEquals "LOGFILES contains /FILE1" "0" "${RES}" $LINENO
assertEquals "two new files" 2 "${#NEW_FILES[@]}" $LINENO

rm "${WATCH_LOG_DIR}/FILE1"
rm "${WATCH_LOG_DIR}/FILE1a"
detect_log_files
assertEquals "no file found" 0 "${#LOGFILES[@]}" $LINENO
assertEquals "two gone files" 2 "${#GONE_FILES[@]}" $LINENO

# create a second file
touch "${WATCH_LOG_DIR}/FILE1"
touch ${WATCH_LOG_DIR}/FILE2
detect_log_files
assertEquals "two files found" 2 "${#LOGFILES[@]}" $LINENO

contains LOGFILES "/FILE2"
RES=$?
assertEquals "LOGFILES contains /FILE2" "0" "${RES}" $LINENO
assertEquals "two new files" 2 "${#NEW_FILES[@]}" $LINENO



# now create a file in a subdir
mkdir -p ${WATCH_LOG_DIR}/subdir
touch ${WATCH_LOG_DIR}/subdir/FILE3
detect_log_files
assertEquals "three files found" 3 "${#LOGFILES[@]}" $LINENO
assertEquals "name is subdir/FILE3" "/subdir/FILE3" "${LOGFILES[2]}" $LINENO

contains LOGFILES "/subdir/FILE3"
RES=$?
assertEquals "LOGFILES contains /subdir/FILE3" "0" "${RES}" $LINENO
assertEquals "1 new file" 1 "${#NEW_FILES[@]}" $LINENO


# a space in one of the directory names
mkdir -p "${WATCH_LOG_DIR}/space dir"
touch "${WATCH_LOG_DIR}/space dir/FILE4"
detect_log_files
assertEquals "four files found" 4 "${#LOGFILES[@]}" $LINENO

contains LOGFILES "/space dir/FILE4"
RES=$?
assertEquals "LOGFILES contains /space dir/FILE4" "0" "${RES}" $LINENO
assertEquals "1 new file" 1 "${#NEW_FILES[@]}" $LINENO



touch "${WATCH_LOG_DIR}/subdir/SPACE FILE"
detect_log_files
assertEquals "5 files found" 5 "${#LOGFILES[@]}" $LINENO


contains LOGFILES "/subdir/SPACE FILE"
RES=$?
assertEquals "LOGFILES contains /subdir/SPACE FILE" "0" "${RES}" $LINENO
assertEquals "1 new file" 1 "${#NEW_FILES[@]}" $LINENO

rm -rf $WATCH_LOG_DIR
