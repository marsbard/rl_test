
cd "`dirname $0`"

source "../funcs.sh"
source "assert.sh"

SLEEP=0.5

#FILE=../../../../test/resource/tinylog.txt

INFILE=/tmp/runtail-test.tmp
OUTFILE=/tmp/errchecker-log.txt

echo -n > $OUTFILE
echo -n > $INFILE

# reset temp file counter
set_count 0
sleep $SLEEP

run_tail $INFILE &

EXC="Exception: this should come through"
echo "$EXC" >> $INFILE
sleep $SLEEP
SIZE=0
RES=`taildiff $OUTFILE $SIZE`
SIZE=`echo $RES | cut -f1 -d:`
DATA=`echo $RES | cut -f2- -d:`
assertContains "exception comes to file" "$EXC" "$DATA" $LINENO

NON="This a non-interesting line"
echo "$NON" >> $INFILE
sleep $SLEEP
RES=`taildiff $OUTFILE $SIZE`
SIZE=`echo $RES | cut -f1 -d:`
DATA=`echo $RES | cut -f2- -d:`
assertEquals "line ignored" "" "$DATA" $LINENO


ERR="ERROR some stuff happened"
echo "$ERR" >> $INFILE
sleep $SLEEP
RES=`taildiff $OUTFILE $SIZE`
SIZE=`echo $RES | cut -f1 -d:`
DATA=`echo $RES | cut -f2- -d:`
assertContains "ERR comes to file" "$ERR" "$DATA" $LINENO


FIN="INFO: Server startup in:"
echo "$FIN" >> $INFILE
sleep $SLEEP
RES=`taildiff $OUTFILE $SIZE`
SIZE=`echo $RES | cut -f1 -d:`
DATA=`echo $RES | cut -f2- -d:`
assertContains "FIN comes to file" "TOMCAT IS STARTED" "$DATA" $LINENO

NUMEXC=`echo $DATA | rev | cut -f1 -d: | rev | xargs`
assertEquals "number of exceptions" "2" "$NUMEXC" $LINENO


TP=`ps ax | grep "tail -F $FILE" | grep -v grep | cut -c1-5`

ps ax | grep $TP

kill $TP $P1
