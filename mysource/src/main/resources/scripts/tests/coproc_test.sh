

PFX=/tmp/coproctest
TMP1=${PFX}_1
TMP2=${PFX}_2

unset CP_TAIL1
unset CP_TAIL2

echo hello > $TMP1
echo wassup > $TMP2

sleep 1

coproc CP_TAIL1 { tail -F $TMP1 ; }
P1=$!
echo $COPROC_PID
coproc CP_TAIL2 { tail -F $TMP2 ; }
P2=$!
echo $COPROC_PID

echo "IN1=${CP_TAIL1[0]}"
echo "OUT1=${CP_TAIL1[1]}"
echo "PID1=$P1"

echo "IN2=${CP_TAIL2[0]}"
echo "OUT2=${CP_TAIL2[1]}"
echo "PID2=$P2"

tail -f <&"${CP_TAIL1[0]}" &

echo walloo >> $TMP1

sleep 5

echo wsdasdalloo >> $TMP1

sleep 1
kill $P1 $P2
