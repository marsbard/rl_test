cd "`dirname $0`"

source "../funcs.sh"
source "assert.sh"


TMP1=/tmp/taildiff_test.1

rm -f $TMP1
touch $TMP1

# first time just sets up current size of file
# should return blank
RET=`taildiff $TMP1 0`
LEN=`echo $RET | cut -f1 -d:`
OUT=`echo $RET | cut -f2- -d:`

assertEquals "first call to taildiff is blank" "" "$OUT" $LINENO
echo "foobar" >> $TMP1

# now we expect to get 'foobar' back
RET=`taildiff $TMP1 $LEN`
LEN=`echo $RET | cut -f1 -d:`
OUT=`echo $RET | cut -f2- -d:`
assertEquals "2nd call gives foobar" "foobar" "$OUT" $LINENO

#assertEquals "get_sz returns 7" "7" "`get_sz $TMP1`" $LINENO

echo "bazbat" >> $TMP1
# now we expect to get 'bazbat' back
RET=`taildiff $TMP1 $LEN`
LEN=`echo $RET | cut -f1 -d:`
OUT=`echo $RET | cut -f2- -d:`
assertEquals "3rd call gives bazbat" "bazbat" "$OUT" $LINENO
