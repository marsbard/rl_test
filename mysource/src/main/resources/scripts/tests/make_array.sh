
cd "`dirname $0`"

source "../funcs.sh"
source "assert.sh"

IN="foo
bar
baz"



make_array "$IN"
assertEquals "first element is foo" "foo" "${_ARR[0]}" $LINENO

ARR=(${_ARR[@]})

assertEquals "third element is baz" "baz" "${ARR[2]}" $LINENO


# testing that blank lines are suppressed
IN="foo

bar


baz"

make_array "$IN"

assertEquals "element 0 is foo" "foo" "${_ARR[0]}" $LINENO
assertEquals "element 1 is bar" "bar" "${_ARR[1]}" $LINENO
assertEquals "element 2 is baz" "baz" "${_ARR[2]}" $LINENO

NEW_FILELIST='/FILE1a
/FILE1'
make_array "${NEW_FILELIST}"
NEW_FILES=(${_ARR[@]})
assertEquals "count new files =2" 2 "${#NEW_FILES[@]}" $LINENO
assertEquals "file 0 is /FILE1a" "/FILE1a" "${NEW_FILES[0]}" $LINENO
assertEquals "file 1 is /FILE1" "/FILE1" "${NEW_FILES[1]}" $LINENO

# spaces in lines should be ok
IN="foo foo
foo bar
foo baz"

make_array "$IN"

assertEquals "element 0 is foo foo" "foo foo" "${_ARR[0]}" $LINENO
assertEquals "element 1 is foo bar" "foo bar" "${_ARR[1]}" $LINENO
assertEquals "element 2 is foo baz" "foo baz" "${_ARR[2]}" $LINENO
