
# https://github.com/costa/dev/blob/master/bash/tag_test.sh
# modified to only output when [ ! -z "$DEBUG" ]
function errcho() {
  if [ ! -z "$DEBUG" ]; then echo >&2 "[$$] $@"; fi
}
function shout() {
  if [ ! -z "$DEBUG" ]
  then
    errcho "$@"
    "$@"
  fi
}


#######################################################################
# Usage: assertEquals "<description>" "<expected>" "<testResult>" <line_no>
# Modified from http://tldp.org/LDP/abs/html/debugging.html
assertEquals ()                 #  If condition false,
{                         #  exit from script
                          #  with appropriate error message.
  E_PARAM_ERR=98
  E_ASSERT_FAILED=99

  errcho "Testing assertion '$1'"

  if [ -z "$4" ]          #  Not enough parameters passed
  then                    #  to assert() function.
    echo assertEquals: not enough params: 1=$1 2=$2 3=$3 4=$4
    return $E_PARAM_ERR   #  No damage done.
  fi

  lineno=$4

  if [ ! "$2" = "$3" ]
  then
    echo "Assertion failed:  \"$1\"  expected \"$2\", got \"$3\""
    echo "File \"$0\", line $lineno"    # Give name of file and line number.
    exit $E_ASSERT_FAILED
    # else
    #   return
    #   and continue executing the script.
  fi

  errcho Assertion passed
}

# Usage: assertContains "<description>" "<contained>" "<within>" <line_no>
assertContains()
{
  DESCR=$1
  CONTAINED=$2
  WITHIN=$3
  LINE=$4

  errcho "Testing assertion '$DESCR'"

  E_PARAM_ERR=98
  E_ASSERT_FAILED=99
  if [ -z "$4" ]          #  Not enough parameters passed
  then                    #  to assert() function.
    echo assertEquals: not enough params: 1=$1 2=$2 3=$3 4=$4
    return $E_PARAM_ERR   #  No damage done.
  fi

  if [[ $WITHIN == *"$CONTAINED"* ]]
  then
    return 0
  fi

  echo "Assertion failed: \"$CONTAINED\" is not contained within \"$WITHIN\""
  exit $E_ASSERT_FAILED
}
