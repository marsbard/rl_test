
cd "`dirname $0`"

source "../funcs.sh"
source "assert.sh"

arr[0]="foo"
arr[1]="bar"
arr[2]="baz"



contains arr foo
RES=$?
assertEquals "arr contains foo" "0" "$RES" $LINENO

contains arr bar
RES=$?
assertEquals "arr contains bar" "0" "$RES" $LINENO

contains arr bat
RES=$?
assertEquals "arr doesn't contain bat" "1" "$RES" $LINENO

contains arr food
RES=$?
assertEquals "arr doesn't contain food" "1" "$RES" $LINENO

# spaces in elements should be ok
IN="foo foo
foo bar
foo baz"

make_array "$IN"

contains _ARR "foo foo"
RES=$?
assertEquals "arr contains 'foo foo'" "0" "$RES" $LINENO

contains _ARR "foo bar"
RES=$?
assertEquals "arr contains 'foo bar'" "0" "$RES" $LINENO

contains _ARR "foo baz"
RES=$?
assertEquals "arr contains 'foo baz'" "0" "$RES" $LINENO


contains _ARR "football"
RES=$?
assertEquals "arr ! contains 'football'" "1" "$RES" $LINENO
