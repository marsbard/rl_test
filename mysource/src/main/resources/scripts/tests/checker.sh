

cd "`dirname $0`"

source "../funcs.sh"
source "assert.sh"


IN="foo"
OUT=`checker $IN`
RES=$?
assertEquals "Normal output dropped" "" "$OUT" $LINENO
assertEquals "RES=0" "0" "$RES" $LINENO

IN="Exception: foo"
OUT=$(checker $IN)
#OUT=`checker < <(echo $IN)`
RES=$?
assertEquals "Exception is copied IN=$IN" "$IN" "$OUT" $LINENO
assertEquals "RES=1" "1" "$RES" $LINENO

IN="not necessarily at line start Exception: foo"
OUT=$(checker $IN)
RES=$?
assertEquals "Exception is copied IN=$IN" "$IN" "$OUT" $LINENO
assertEquals "RES=1" "1" "$RES" $LINENO

IN="ERROR foo"
OUT=`checker $IN`
RES=$?
assertEquals "Exception is copied IN=$IN" "$IN" "$OUT" $LINENO
assertEquals "RES=1" "1" "$RES" $LINENO

IN="INFO: Server startup in:"
OUT=`checker $IN`
RES=$?
assertEquals "RES=2" "2" "$RES" $LINENO
assertEquals "TOMCAT IS STARTED" "TOMCAT IS STARTED" "$OUT" $LINENO

IN="blah blah blah INFO:"
OUT=`checker $IN`
RES=$?
assertEquals "Normal output dropped" "" "$OUT" $LINENO
assertEquals "RES=0" "0" "$RES" $LINENO

IN="Exception: this should come through"
OUT=`checker $IN`
RES=$?
assertEquals "Exception is copied IN=$IN" "$IN" "$OUT" $LINENO
assertEquals "RES=1" "1" "$RES" $LINENO

COUNT=0
CHECK=""
while read LINE
do
  OUT=`checker $LINE`
  RES=$?
  if [ $RES = 1 ]; then COUNT=$(( $COUNT + 1 )); fi
  if [ $RES = 2 ]
  then
    OUT="$OUT - Number of exceptions: $COUNT"
    COUNT=0
  fi
  if [ $RES != 0 ]
  then
    if [ -z "$CHECK" ]
    then
      CHECK=$OUT
    else
      CHECK=`printf "$CHECK\n$OUT"`
    fi
  fi
  #echo "CHECK=$CHECK"
done < <(cat ../../../../test/resource/tinylog.txt)

EXPECTED="ERROR Something failed and you will never believe what happened next.
Exception: 20 errors you never thought you'd see on your server.
Exception: This one weird trick brought your server down
TOMCAT IS STARTED - Number of exceptions: 3"

assertEquals "expected output" "$EXPECTED" "$CHECK" $LINENO
