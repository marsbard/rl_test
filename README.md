# rl_test [![Build Status](https://travis-ci.org/marsbard/rl_test.svg?branch=master)](https://travis-ci.org/marsbard/rl_test)

```
  cd mysource
  mvn package
  cd ${WORK_DIR}
  tar xzvf ${INSTALL_DIR}/mysource/target/project.tar.gz
  ./project/bin/errchecker
```
On startup it makes the watched folder `/tmp/tomcat-logs` if it doesn't already exist

When a new file appears in that folder, or any subfolder of that folder, a new tail 
process is started to watch it. When certain recognised patterns are found in the 
input, then output is sent to `/tmp/errchecker-log.txt`

To start with it will tail up to 50 lines, and then follow each file. If any logs are 
likely to pick up more lines than that within the first second of their lifetime then
either `$TAILCMD` can be modified to tail more than 50 lines initially or else
`LOOPSLEEP` can be set to a decimal fraction rather than 1 second, which most sleep
implementations should support. Both these envars are available at the top of 
`errchecker`

You can always look at [NOTES.txt](https://github.com/marsbard/rl_test/blob/master/mysource/src/main/resources/docs/NOTES.txt)

You can also run the tests if you wish
```
  cd ${INSTALL_DIR}/mysource
  ./src/main/resources/scripts/tests/runtests.sh
```

or else you can look at the results of [Travis running them](https://travis-ci.org/marsbard/rl_test)

